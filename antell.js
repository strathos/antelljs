var	request = require('request'),
	cheerio = require('cheerio'),
	restify = require('restify'),
	server = restify.createServer(),
	url = "http://www.antell.fi/lounaslistat/lounaslista.html?owner=128",
	trimWhiteSpace = function(toBeTrimmed) {
		return toBeTrimmed.replace(/\s{2,}/g, " ").trim();
	},
	splitByLanguage = function(toBeSplitted, language) {
		var splittedMenu = [];
		for (var i in toBeSplitted) {
			tmp = toBeSplitted[i].split(/\s\/\s|\s\(/)[language];
			attr = toBeSplitted[i].split(/\(|\)/)[1];
			if (attr) {
				tmp = tmp + " " + attr.replace(/\s/g, "")
			}
			splittedMenu.push(tmp);
		}
		return splittedMenu;
	},
	printList = function (url, callBack) {
		request(url, function(err, resp, body) {
			if (!err && resp.statusCode == 200) {
				var	menu = [],
					data = {},
					week = "",
					mon = [], monFi = [], monEn = [],
					tue = [], wed = [], thu = [], fri = [], spe = "";
				var $ = cheerio.load(body);
				$('span.title', 'td.main-title').each(function() {
					var text = trimWhiteSpace($(this).text());
					if (text.length) {
						week = text.replace(/[^0-9]/g,"");
					}
				});
				$('td[align="left"]', 'td.outer').each(function() {
					var text = trimWhiteSpace($(this).text());
					if (text.length) {
						menu.push(text);
					}
				});
				for (var i in menu) {
					if (menu[i].substr(0,3) == "VL ") {
						var friEnd = i;
					}
					if (menu[i].substr(0,6) == "Week A") {
						var weekSpecial = i;
					}
				}
				mon = menu.slice(menu.indexOf("Maanantai")+1, menu.indexOf("Tiistai"));
				tue = menu.slice(menu.indexOf("Tiistai")+1, menu.indexOf("Keskiviikko"));
				wed = menu.slice(menu.indexOf("Keskiviikko")+1, menu.indexOf("Torstai"));
				thu = menu.slice(menu.indexOf("Torstai")+1, menu.indexOf("Perjantai"));
				fri = menu.slice(menu.indexOf("Perjantai")+1, friEnd);
				if (weekSpecial) {
					spe = menu[weekSpecial].substr(0, menu[weekSpecial].indexOf("*")).replace(/ week wok/gi, "").split(": ").slice(1);
				}

				data = {
						"week" : week,
						"menu" : {
							"fi" : {
								"mon" : { "Maanantai": splitByLanguage(mon, 0)},
								"tue" : { "Tiistai": splitByLanguage(tue, 0)},
								"wed" : { "Keskiviikko": splitByLanguage(wed, 0)},
								"thu" : { "Torstai": splitByLanguage(thu, 0)},
								"fri" : { "Perjantai": splitByLanguage(fri, 0)},
								"spe" : spe
							},
							"en" : {
								"mon" : { "Monday": splitByLanguage(mon, 1)},
								"tue" : { "Tuesday": splitByLanguage(tue, 1)},
								"wed" : { "Wednesday": splitByLanguage(wed, 1)},
								"thu" : { "Thursday": splitByLanguage(thu, 1)},
								"fri" : { "Friday": splitByLanguage(fri, 1)},
								"spe" : spe
							}
						}
					};
				callBack(data);
			}
		});
	};
server.pre(restify.pre.sanitizePath());

server.get('/api', function(req, res, next) {
	printList(url, function(data) {
		res.send(200,data);
	});
	return next();
});

server.get('/api/:lang', function(req, res, next) {
	var lang = req.params.lang.toLowerCase();
	if (lang == "fi" || lang == "en") {
		printList(url, function(data) {
			res.send(200,data.menu[lang]);
		});
	} else {
		res.send(200,JSON.parse('{"error":"Requested language not found."}'));
	}
	return next();
});

server.get('/api/:lang/:day', function(req, res, next) {
	var lang = req.params.lang.toLowerCase(),
		day = req.params.day.toLowerCase();
	if (lang == "fi" || lang == "en") {
		if (day == "mon" || day == "tue" || day == "wed" || day == "thu" || day == "fri") {
			printList(url, function(data) {
				res.send(200,data.menu[lang][day]);
			});
		} else {
			res.send(200,JSON.parse('{"error":"Requested day not found."}'));
		}
	} else {
		res.send(200,JSON.parse('{"error":"Requested language not found."}'));
	}
	return next();
});

server.listen(3000, function() {
	console.log('%s listening at %s', server.name, server.url);
});
