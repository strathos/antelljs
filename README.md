# AntellJS
A Node.js application to provide the lunch menu from Antell's restaurant in LMF via a REST API in JSON.

### Dependencies:
* [Cheerio](http://cheeriojs.github.io/cheerio/) for scraping the website
* [Request](https://github.com/request/request) for downloading the website source
* [Restify](http://mcavage.me/node-restify/) for providing the web server functionality and REST API

### Routes include so far:

Route                   | Explanation
-----                   | -----------
/api                    | The whole menu
/api/[language]         | Menu in requested language only (fi or en)
/api/[language]/[day]   | Menu in requested language only for a specific day in week (mon, tue, wed, thu, fri)

### How to use
Start by cloning the repository:
> git clone git@bitbucket.org:strathos/antelljs.git

Start the web server:
> node antell.js

__Optionally__ you can use [Nodemon](http://nodemon.io/) to run the server to gain automatic reloading when file changes are detected (mainly __for development use__):
> npm install -g nodemon

> nodemon antell.js

By default the server will run on port 3000. Use curl or REST client to test the server:
> curl http://127.0.0.1:3000/api

### Todo list:
* [ ] Use MongoDB to store the menu instead of scraping and parsing it every time
* [ ] Add the possibility to request a menu based on the number of the week from the DB
* [ ] Maybe create an admin panel to handle the data in DB
